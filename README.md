libcf++
=====

1. Exception stack
  1. Getting System Error Code and Message
1. Binary data
1. String Formatter
1. Memory allocator
  1. supports Verification
1. File I/O
1. TCP Socket & NIC utility
1. Logging (advisor: vfire)
1. Utility Code for Debugging
1. Thread & Mutex
1. Codec
  1. hex encoding/decoding
  1. base64 encoding/decoding
1. Utilities
  1. Date&Time
1. Test macros
1. supports JNI

..more needs ?
