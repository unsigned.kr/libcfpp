/**
 * @file util.cpp
 * @author myusgun@gmail.com
 * @brief util
 */
#include "cf/util.h"

#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <string.h>

#ifdef _ON_WINDOWS
# include <windows.h>
# include <sys/timeb.h>
#else
# include <sys/time.h>
# include <sys/types.h>
# include <unistd.h>
#endif

/*--------------------------------------------------------------*/
static const cf::char_t * getWeekName(const cf::int32_t week)
{
	static const cf::char_t * weekday[] = {
		"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"
	};

	return weekday[week];
}
/*--------------------------------------------------------------*/

cf::util * cf::util::getInstance()
{
	static cf::util instance;

	return &instance;
}

cf::util::datetime cf::util::getDateTime() const
	throw (cf::exception)
{
	cf::util::datetime dt;
	struct timeval timeVal;
	struct tm * timebuf = NULL;

#ifdef _ON_WINDOWS
	__time64_t	t64;
	__timeb64	tb64;

	_time64 (&t64);
	_ftime64 (&tb64);
	timebuf = _localtime64 (&t64);
	timeVal.tv_usec = tb64.millitm;
#else
	gettimeofday(&timeVal, NULL);
	timebuf = localtime((const time_t *)&timeVal.tv_sec);
	timeVal.tv_usec /= 1000;
#endif
	if (!timebuf)
		THROW_EXCEPTION("cannot get localtime(" << errno
						<< ":" << strerror(errno) << ")");

	dt.mYear		= timebuf->tm_year + 1900;
	dt.mMonth		= timebuf->tm_mon + 1;
	dt.mDay			= timebuf->tm_mday;
	dt.mWeek		= timebuf->tm_wday;
	dt.mWeekName	= getWeekName(dt.mWeek);

	dt.mHour		= timebuf->tm_hour;
	dt.mMin			= timebuf->tm_min;
	dt.mSec			= timebuf->tm_sec;

	dt.mUsec		= (cf::int32_t)timeVal.tv_usec;

	return dt;
}

cf::int64_t cf::util::getTimeStamp() const
{
#ifdef _ON_WINDOWS
	return _time64(NULL);
#else
	return time(NULL);
#endif
}
