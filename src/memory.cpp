/**
 * @file memory.cpp
 * @author myusgun@gmail.com
 * @brief memory
 */
#include "cf/memory.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const cf::uint8_t gVerifier[4] = {0xcf, '+', '+', 0x00};

cf::memory::memory(const cf::size_t size)
	throw (cf::exception)
	: mMemory(NULL)
{
	mMemory = cf::memory::alloc(size);
}

cf::memory::~memory()
{
	cf::memory::free(mMemory);
	mMemory = NULL;
}

cf::void_t * cf::memory::buffer()
{
	return mMemory;
}

cf::void_t * cf::memory::alloc(const cf::size_t size)
	throw (cf::exception)
{
	cf::uint8_t * mem = (cf::uint8_t *)::calloc(size + sizeof(gVerifier), 1);
	if (!mem)
		THROW_EXCEPTION("cannot allocate memory");

	::memcpy(mem, gVerifier, sizeof(gVerifier));

	return reinterpret_cast<cf::void_t *>(mem + sizeof(gVerifier));
}

cf::void_t cf::memory::free(cf::void_t * mem)
	throw (cf::exception)
{
	if (!valid(mem))
		THROW_EXCEPTION("invalid address or verification code");

	cf::uint8_t * ptr = reinterpret_cast<cf::uint8_t *>(mem);
   
	ptr -= sizeof(gVerifier);

	::memset(ptr, 0x00, sizeof(gVerifier));

	::free(ptr);
}

cf::bool_t cf::memory::valid(const cf::void_t * mem)
{
	const cf::uint8_t * ptr = reinterpret_cast<const cf::uint8_t *>(mem);

	/* include checking NULL */
	if (ptr < (cf::uint8_t *)sizeof(gVerifier))
		return false;

	ptr -= sizeof(gVerifier);

	if (::memcmp(ptr, gVerifier, sizeof(gVerifier)))
		return false;

	return true;
}
