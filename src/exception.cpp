/**
 * @file exception.cpp
 * @author myusgun@gmail.com
 * @brief exception
 */
#include "cf/exception.h"

#include <string.h>

#ifdef _ON_WINDOWS
# include <windows.h>
#else
# include <errno.h>
#endif

cf::exception::exception(const cf::char_t * message,
						 const cf::char_t * file,
						 const cf::int32_t line,
						 const cf::char_t * func)
	: mMessage(message),
	  mFile(file),
	  mLine(line),
	  mFunction(func)
{
#ifndef _HPUX
	mStack.push_back(*this);
#endif
}

cf::exception::exception(const cf::exception & ex,
						 const cf::char_t * file,
						 const cf::int32_t line,
						 const cf::char_t * func)
	: mMessage(ex.what()),
	  mFile(file),
	  mLine(line),
	  mFunction(func)
{
#ifndef _HPUX
	mStack = ex.mStack;
	mStack.push_back(*this);
#endif
}

cf::exception::~exception()
	throw ()
{
}

const cf::char_t * cf::exception::what() const
	throw ()
{
	return mMessage.c_str();
}

const cf::char_t * cf::exception::file() const
{
	return mFile.c_str();
}

cf::int32_t cf::exception::line() const
{
	return mLine;
}

const cf::char_t * cf::exception::function() const
{
	return mFunction.c_str();
}

std::string cf::exception::stackTrace() const
{
#ifdef _HPUX
	return mMessage;
#else
	cf::uint32_t iter = 0;
	cf::formatter fmt;

	fmt <<  mStack[0].what();
	for ( ; iter < mStack.size() ; iter++)
	{
		const cf::exception & ex = mStack[iter];

		fmt << "\n "
			<< "#" << iter << " "
			<< "[" << ex.file() << ":" << ex.line() << "]"
			<< "[" << ex.function() << "()]";
	}
	fmt << "\n";

	return fmt.str();
#endif
}

cf::int32_t cf::exception::systemCode()
{
#ifdef _ON_WINDOWS
# define SYSERR()	GetLastError()
#else
# define SYSERR()	errno
#endif
	return static_cast<cf::int32_t>(SYSERR());
}

std::string cf::exception::systemMessage()
{
	std::string msg;

#ifdef _ON_WINDOWS
	cf::char_t * ptr = NULL;
	cf::size_t size = 0;
	const cf::uint32_t flag = FORMAT_MESSAGE_ALLOCATE_BUFFER
							| FORMAT_MESSAGE_FROM_SYSTEM
							| FORMAT_MESSAGE_IGNORE_INSERTS;
	const cf::uint32_t langID = MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT);

	size = FormatMessageA(flag, NULL, systemCode(), langID, (LPSTR)&ptr, 0, NULL);

	if (ptr && size > 0)
	{
		msg.assign(ptr, size);
		LocalFree(ptr);
	}
#else
	msg = strerror(systemCode());
#endif

	return msg;
}
