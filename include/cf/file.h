/**
 * @file file.h
 * @author myusgun@gmail.com
 * @brief file
 */
#ifndef __cf_file_h__
#define __cf_file_h__

#include "cf/exception.h"
#include "cf/bin.h"

#include <string>
#include <list>

namespace cf
{
	/**
	 * file
	 */
	class file
	{
	public:
		/** file flag */
		enum FILE_FLAG
		{
			READ	= 0x00000001,	/**< read-only */
			WRITE	= 0x00000002,	/**< write-only */
			RW		= 0x00000004,	/**< read & write */
			CREATE	= 0x00000008,	/**< create */
			TRUNC	= 0x00000010,	/**< truncate */
			APPEND	= 0x00000020,	/**< append */
			EXCL	= 0x00000040,	/**< exclusive */
			LOCK	= 0x00010000	/**< lock */
		};

		/** whence for seeking */
		enum FILE_WHENCE
		{
			BEGIN,		/**< from begin */
			CURRENT,	/**< from current position */
			END			/**< from end */
		};

		/** type of dir/file entry */
		enum ENTRY_TYPE
		{
			UNDEFINED,		/**< undefined in file::ENTRY_TYPE */
			DIRECTORY,		/**< directory */
			REGULAR_FILE	/**< regular file */
		};

		/** dir/file entry */
		typedef struct ENTRY
		{
			std::string mName;	/**< name */
			ENTRY_TYPE mType;	/**< @see ENTRY_TYPE */
		} ENTRY;

		typedef std::list<file::ENTRY> EntryList;

	private:
		cf::int32_t mFD;
		std::string mPath;
		cf::bool_t mIsLocked;

	public:
		/**
		 * constructor
		 * @param path file path
		 * @throw cf::exception
		 */
		file(const cf::char_t * path)
			throw (cf::exception);

		/**
		 * destructor
		 */
		virtual ~file();

		/**
		 * get path delimiter of system
		 * @return delimiter
		 * @remarks on windows, "\\"; on *nix, "/"
		 */
		static const cf::char_t * getDelimiter();

		/**
		 * open
		 * @param flag flag
		 * @throw cf::exception
		 */
		cf::void_t open(const cf::int32_t flag)
			throw (cf::exception);

		/**
		 * close
		 * @throw cf::exception
		 * @remarks if lock-file was not removed, an exception is thrown
		 */
		cf::void_t close()
			throw (cf::exception);

		/**
		 * is opened ?
		 * @return true; false
		 */
		cf::bool_t isOpened();

		/**
		 * set position
		 * @return offset
		 * @param offset offset
		 * @param whence whence
		 * @throw cf::exception
		 */
		cf::int32_t seek(const cf::int32_t offset,
						 const cf::int32_t whence = cf::file::BEGIN)
			throw (cf::exception);

		/**
		 * get current file position
		 * @return get position
		 * @throw cf::exception
		 */
		cf::int32_t tell()
			throw (cf::exception);

		/**
		 * get size
		 * @return file size
		 * @throw cf::exception
		 */
		cf::int32_t size()
			throw (cf::exception);

		/**
		 * exists ?
		 * @return true; false
		 */
		cf::bool_t exists() const;

		/**
		 * read
		 * @param out a buffer, allocated ahead
		 * @throw cf::exception
		 */
		cf::void_t read(bin & out)
			throw (cf::exception);

		/**
		 * read
		 * @return RawStub
		 * @param len read bytes
		 * @throw cf::exception
		 */
		bin read(const cf::int32_t len = 0)
			throw (cf::exception);

		/**
		 * write
		 * @param in RawStub instance
		 * @throw cf::exception
		 */
		cf::void_t write(const bin & in)
			throw (cf::exception);

		/**
		 * remove file
		 * @throw cf::exception
		 */
		cf::void_t remove()
			throw (cf::exception);

		/**
		 * rename file
		 * @param path new path
		 * @throw cf::exception
		 */
		cf::void_t rename(const cf::char_t * path)
			throw (cf::exception);

		/**
		 * get path
		 * @return path
		 */
		const cf::char_t * getPath() const;

		/**
		 * is locked ?
		 * @return true; false
		 */
		cf::bool_t isLocked() const;

		/**
		 * make directory
		 * @param path directory path
		 * @throw cf::exception
		 */
		static cf::void_t makedir(const cf::char_t * path)
			throw (cf::exception);

		/**
		 * get directory entry-list
		 * @param path directory entry-list
		 * @param exceptDots except . and .. from list ? (as default, true)
		 * @throw cf::exception
		 * @see EntryList, ENTRY, ENTRY_TYPE
		 */
		static EntryList getEntryList(const cf::char_t * path,
									  const cf::bool_t exceptDots = true)
			throw (cf::exception);
	};
}

#endif // #ifndef __cf_file_h__
