/**
 * @file util.h
 * @author myusgun@gmail.com
 * @brief util
 */
#ifndef __cf_util_h__
#define __cf_util_h__

#include "cf/exception.h"

namespace cf
{
	/**
	 * util
	 */
	class util
	{
	public:
		/** date/time/week */
		typedef struct datetime
		{
			cf::int32_t mYear;		/**< year */
			cf::int32_t mMonth;		/**< month */
			cf::int32_t mDay;		/**< day */
			cf::int32_t mWeek;		/**< weekday (0:SUN ~ 6:SAT) */
			std::string mWeekName;	/**< string representation of weekday (SUN ~ SAT) */

			cf::int32_t mHour;		/**< hour */
			cf::int32_t mMin;		/**< minute */
			cf::int32_t mSec;		/**< second */
			cf::int32_t mUsec;		/**< micro second */
		} datetime;

	public:
		/**
		 * get instance
		 * @return instance
		 */
		static util * getInstance();

		/**
		 * get current time
		 * @return datetime structure
		 * @throw cf::exception
		 * @see struct datetime
		 */
		datetime getDateTime() const
			throw (cf::exception);

		/**
		 * get current timestamp
		 * @return time_t
		 */
		cf::int64_t getTimeStamp() const;
	};
}

#endif // #ifndef __cf_util_h__
