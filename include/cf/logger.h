/**
 * @file logger.h
 * @author myusgun@gmail.com
 * @brief logger
 */
#ifndef __cf_logger_h__
#define __cf_logger_h__

#include "cf/exception.h"
#include "cf/task.h"
#include "cf/util.h"

#include <stdio.h>

#include <string>
#include <vector>

namespace cf
{
	/**
	 * singleton logger
	 */
	class logger
	{
	private:
		/* log file management */
		std::string mPath;
		cf::int32_t mLogLevel;
		task::mutex mOpenMutex;
		task::mutex mRenameMutex;

		typedef struct _LOG_CTX
		{
			cf::int32_t mOption;
			struct
			{
				cf::int32_t mMaxLogSize;		/**< mega-bytes */
				cf::int32_t mCurrentDateTime;	/**< yyyymmdd */
			} mOptionParam;

			std::string mPrefix;
			std::string mDescription;
			std::string mFullPath;
			FILE * mFP;
		} LOG_CTX;

		/* level == index-of-vector */
		std::vector<LOG_CTX> mLogPool;

		/**
		 * constructor
		 * @throw cf::exception
		 */
		logger()
			throw (cf::exception);

		/**
		 * destructor
		 */
		virtual ~logger();

		/**
		 * open log file
		 * @param level level to open file
		 * @throw cf::exception
		 */
		cf::void_t open(const cf::int32_t level)
			throw (cf::exception);

		/**
		 * rename for rotation
		 * @param level log level
		 * @throw cf::exception
		 */
		cf::void_t rename(const cf::int32_t level)
			throw (cf::exception);

		/**
		 * overwrite newfp to fp for fullpath
		 * @param prefix prefix of fileprefix
		 * @param option option
		 * @param newfp new fp
		 */
		cf::void_t replace(const std::string & prefix,
						   const cf::int32_t option,
						   FILE * newfp);

		/**
		 * date rotation
		 * @param level log level
		 * @param dt datetime
		 * @throw cf::exception
		 */
		cf::void_t rotateByDate(const cf::int32_t level,
								const util::datetime & dt)
			throw (cf::exception);

		/**
		 * size rotation
		 * @param level log level
		 * @throw cf::exception
		 */
		cf::void_t rotateBySize(const cf::int32_t level)
			throw (cf::exception);

	public:
		/**
		 * log option
		 */
		enum _LOG_OPTION
		{
			NO_OPTION	= 0x00000000,	/**< no option */
			FORCED		= 0x00000001,	/**< forced write */
			DAILY		= 0x00000010,	/**< daily rotation */
			SIZE		= 0x00000020,	/**< size rotation */
			PID			= 0x00000100	/**< process-id in log file name */
		};

		/**
		 * get instance
		 * @return global instance
		 */
		static logger * getInstance();

		/**
		 * initialize
		 * @param path directory path for log
		 * @throw cf::exception
		 */
		cf::void_t init(const std::string & path);

		/**
		 * add log level
		 * @param prefix file prefix
		 * @param level log level
		 * @param description description for log level
		 * @param option option(i.e., forced, rotation)
		 * @param rotationSize rotation parameter(i.e., size)
		 * @throw cf::exception
		 * @see _LOG_OPTION
		 * @remarks start from 1
		 */
		cf::void_t add(const std::string & prefix,
					   const cf::int32_t level,
					   const std::string & description,
					   const cf::int32_t option,
					   const cf::int32_t rotationSize = 0)
			throw (cf::exception);

		/**
		 * get log level
		 * @return log level
		 */
		cf::int32_t getLevel();

		/**
		 * set log level
		 * @param level log level
		 */
		cf::void_t setLevel(const cf::int32_t level);

		/**
		 * get file path
		 * @return file path
		 * @param level log level
		 * @throw cf::exception
		 */
		std::string getPath(const cf::int32_t level) const
			throw (cf::exception);

		/**
		 * is log level registered ?
		 * @return true; false
		 * @param level log level
		 */
		cf::bool_t isRegistered(const cf::int32_t level) const;

		/**
		 * is log level enabled ?
		 * @return true; false
		 * @param level log level
		 */
		cf::bool_t isEnabled(const cf::int32_t level) const;

		/**
		 * write
		 * @param level level
		 * @param msg message
		 */
		cf::void_t write(const cf::int32_t level,
						 const std::string & msg);
	};
}

#endif // #ifndef __cf_logger_h__
