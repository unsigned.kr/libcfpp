/**
 * @file jni.h
 * @author myusgun@gmail.com
 * @brief JNI
 */
#ifndef __cf_jni_h__
#define __cf_jni_h__

#include "cf/bin.h"
#include "cf/exception.h"

#include <jni.h>
#include <string>

namespace cf
{
	/**
	 * jni
	 */
	class jni
	{
	private:
		JNIEnv * mJNIEnv;

		cf::void_t releaseString(jstring jinput,
								 cf::char_t * in);

		cf::void_t releaseByteArray(jbyteArray jinput,
									cf::char_t * in);

		cf::void_t releaseLocalMemory(jobject in);

	public:
		/* from java */
		bin getByteArray(jbyteArray jinput)
			throw (cf::exception);

		std::string getString(jstring jinput)
			throw (cf::exception);

		/* to java */
		cf::void_t throwException(const char * message);

		jbyteArray newByteArray(const bin & in);

		jstring newString(const std::string & in);
	};
}

#endif // #ifndef __cf_jni_h__
