/**
 * @file exception.h
 * @author myusgun@gmail.com
 * @brief exception
 */
#ifndef __cf_exception_h__
#define __cf_exception_h__

#include "cf/types.h"
#include "cf/formatter.hpp"

#include <string>
#include <vector>
#include <exception>

#define _SOURCE_INFO			__FILE__, __LINE__, __func__
#define THROW_EXCEPTION(_m)		throw cf::exception(STR(_m).c_str(), _SOURCE_INFO)
#define FORWARD_EXCEPTION(_e)	throw cf::exception(_e, _SOURCE_INFO)

namespace cf
{
	/**
	 * exception
	 */
	class exception : public std::exception
	{
	private:
		std::string mMessage;
		std::string mFile;
		cf::int32_t mLine;
		std::string mFunction;
#ifndef _HPUX
		std::vector<exception> mStack;
#endif

	public:
		/**
		 * constructor
		 * @param message error message
		 * @param file source filename
		 * @param func function
		 * @param line source line
		 * @see THROW_EXCEPTION
		 */
		exception(const cf::char_t * message,
				  const cf::char_t * file,
				  const cf::int32_t line,
				  const cf::char_t * func);

		/**
		 * constructor(for stack)
		 * @param ex previous exception
		 * @param file source filename
		 * @param func function
		 * @param line source line
		 * @see FORWARD_EXCEPTION
		 * @remarks make thrown-stack
		 */
		exception(const exception & ex,
				  const cf::char_t * file,
				  const cf::int32_t line,
				  const cf::char_t * func);

		/**
		 * destructor derived from std::exception::~exception()
		 */
		virtual ~exception()
			throw ();

		/**
		 * std::exception::what()
		 * @return error message
		 */
		const cf::char_t * what() const
			throw ();

		/**
		 * get source file name
		 * @return source file name
		 */
		const cf::char_t * file() const;

		/**
		 * get source line
		 * @return source line
		 */
		cf::int32_t line() const;

		/**
		 * get function name
		 * @return function name
		 */
		const cf::char_t * function() const;

		/**
		 * get stack of thrown-exceptions
		 * @return stack of thrown-exceptions
		 */
		std::string stackTrace() const;

		/**
		 * get GetLastError() in windows; otherwise, errno
		 * @return system error code
		 */
		static cf::int32_t systemCode();

		/**
		 * get error message for system error code
		 * @return error message for system error code
		 */
		static std::string systemMessage();
	};
}

#endif // #ifndef __cf_exception_h__
