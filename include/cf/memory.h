/**
 * @file memory.h
 * @author myusgun@gmail.com
 * @brief memory
 */
#ifndef __cf_memory_h__
#define __cf_memory_h__

#include "cf/types.h"
#include "cf/exception.h"

namespace cf
{
	/**
	 * memory
	 */
	class memory
	{
	private:
		cf::void_t * mMemory;

	public:
		/**
		 * constructor. allocate memory
		 * @param size memory size
		 * @throw cf::exception
		 */
		memory(const cf::size_t size = 0)
			throw (cf::exception);

		/**
		 * destructor. free memory
		 */
		virtual ~memory();

		/**
		 * get buffer memory address
		 */
		cf::void_t * buffer();

		/**
		 * static member function to get an address of allocated memory
		 * @param size memory size
		 * @throw cf::exception
		 */
		static cf::void_t * alloc(const cf::size_t size)
			throw (cf::exception);

		/**
		 * static member function to free a memory allocated from cf::memory::alloc()
		 * @param mem memory address
		 * @throw cf::exception
		 */
		static cf::void_t free(cf::void_t * mem)
			throw (cf::exception);

		/**
		 * static member function to check the memory is valid
		 * @return true; false
		 * @param mem memory address
		 */
		static cf::bool_t valid(const cf::void_t * mem);
	};
}

#endif // #ifndef __cf_memory_h__
