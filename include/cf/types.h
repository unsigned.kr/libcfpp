/**
 * @file types.h
 * @author myusgun@gmail.com
 * @brief common header
 */
#ifndef __cf_types_h__
#define __cf_types_h__

#if defined(WIN32) || defined(_WIN32) || defined(_WIN64)
# define _ON_WINDOWS
# define __func__	__FUNCTION__
# if _MSC_VER < 1900
#  define snprintf	_snprintf
# endif
#endif

namespace cf
{
	typedef void				void_t;
	typedef char				char_t;
	typedef unsigned char		uchar_t;
	typedef signed char			int8_t;
	typedef uchar_t				uint8_t;
	typedef signed short		int16_t;
	typedef unsigned short		uint16_t;
	typedef signed int			int32_t;
	typedef unsigned int		uint32_t;
	typedef long				long_t;
	typedef unsigned long		ulong_t;
	typedef bool				bool_t;
#ifdef _ON_WINDOWS
	typedef signed __int64		int64_t;
	typedef unsigned __int64	uint64_t;
#else
	typedef signed long long	int64_t;
	typedef unsigned long long	uint64_t;
#endif

#ifdef _ON_WINDOWS
	typedef uint32_t			size_t;
# ifdef _WIN64
	typedef uint64_t			socket_t;
# else
	typedef uint32_t			socket_t;
# endif
#else
	typedef ulong_t				size_t;
	typedef int32_t				socket_t;
#endif

	const cf::int32_t			nodesc = -1;
};

#endif // #ifndef __cf_types_h__
