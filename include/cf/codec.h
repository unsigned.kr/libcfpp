/**
 * @file codec.h
 * @author myusgun@gmail.com
 * @brief codec
 */
#ifndef __cf_codec_h__
#define __cf_codec_h__

#include "cf/bin.h"
#include "cf/exception.h"

#include <string>

namespace cf
{
	/**
	 * codec
	 */
	namespace codec
	{
		/**
		 * interface
		 */
		class ICodec
		{
		public:
			/**
			 * encode(virtual)
			 * @return encoded string
			 * @param in input
			 * @throw cf::exception
			 */
			virtual std::string encode(const bin & in) const
				throw (cf::exception) = 0;

			/**
			 * decode(virtual)
			 * @return decoded binary
			 * @param in input
			 * @throw cf::exception
			 */
			virtual bin decode(const std::string & in) const
				throw (cf::exception) = 0;
		};

		/**
		 * base64 encoder/decoder
		 */
		class base64 : public ICodec
		{
		public:
			/**
			 * get instance
			 * @return base64 instance
			 */
			static base64 * getInstance();

			/**
			 * base64 encode
			 * @return base64 encoded string
			 * @param in input
			 * @throw cf::exception
			 */
			std::string encode(const bin & in) const
				throw (cf::exception);

			/**
			 * base64 decode
			 * @return base64 decoded binary
			 * @param in base64 string
			 * @throw cf::exception
			 */
			bin decode(const std::string & in) const
				throw (cf::exception);
		};

		/**
		 * hex encoder/decoder
		 */
		class hex : public ICodec
		{
		public:
			/**
			 * get instance
			 * @return hex instance
			 */
			static hex * getInstance();

			/**
			 * hex encode
			 * @return hex encoded string
			 * @param in input
			 * @throw cf::exception
			 */
			std::string encode(const bin & in) const
				throw (cf::exception);

			/**
			 * hex decode
			 * @return hex decoded binary
			 * @param in hex string
			 * @throw cf::exception
			 */
			bin decode(const std::string & in) const
				throw (cf::exception);
		};
	};
}

#endif // #ifndef __cf_codec_h__
