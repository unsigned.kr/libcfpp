/**
 * @file bin.h
 * @author myusgun@gmail.com
 * @brief bin
 */
#ifndef __cf_bin_h__
#define __cf_bin_h__

#include "cf/types.h"
#include "cf/exception.h"

#include <vector>
#include <string>
#include <stdio.h>

namespace cf
{
	/**
	 * binary
	 */
	class bin
	{
	private:
		typedef std::vector<cf::uint8_t> bin_t;

		bin_t mBin;

	public:
		/**
		 * shallow-copy constructor(default constructor)
		 * @param in binary
		 * @param length binary size
		 */
		bin(const cf::uint8_t * in = NULL,
			const cf::size_t length = 0);

		/**
		 * asciiz-byte shallow-copy constructor
		 * @param in asciiz-byte
		 */
		bin(const std::string & in);

		/**
		 * copy constructor
		 * @param in an instance of bin(not constant)
		 */
		bin(const bin & in);

		/**
		 * reset binary. fill with zero
		 */
		cf::void_t clear();

		/**
		 * get binary
		 * @return address of binary
		 */
		cf::uint8_t * buffer() const;

		/**
		 * get binary size
		 * @return size of binary
		 */
		cf::size_t size() const;

		/**
		 * resize
		 * @param size new size
		 */
		cf::void_t resize(const cf::size_t size);

		/**
		 * set 
		 * @param in data
		 * @param length size
		 */
		cf::void_t set(const cf::uint8_t * in,
					   const cf::size_t length);

		/**
		 * copy to rear of buffer
		 * @param in binary
		 * @param appendedSize binary size
		 */
		cf::void_t append(const cf::uint8_t * in,
						  const cf::size_t appendedSize);

		/**
		 * copy to rear of buffer
		 * @param in bin instance
		 * @see bin::operator +=()
		 */
		cf::void_t append(const bin & in);

		/**
		 * compare binary
		 * @return true; false
		 * @param in bin instance
		 * @see bin::operator ==()
		 */
		cf::bool_t equal(const bin & in) const;

		/**
		 * find in binary
		 * @return on success, position; otherwise, (cf::size_t)-1
		 * @param in binary
		 * @param length binary size
		 */
		cf::size_t find(const cf::uint8_t * in,
						const cf::size_t length) const;

		/**
		 * set
		 * @param in bin
		 */
		bin & operator =(const bin & in);

		/**
		 * set
		 * @param in string
		 */
		bin & operator =(const std::string & in);

		/**
		 * copy to rear of buffer
		 * @param in bin instance
		 * @see bin::append()
		 */
		cf::void_t operator +=(const bin & in);

		/**
		 * combine binary
		 * @return combined instance
		 */
		bin operator +(const bin & in) const;

		/**
		 * compare binary
		 * @return true; false
		 * @param in bin instance
		 * @see bin::equal()
		 */
		cf::bool_t operator ==(const bin & in) const;

		/**
		 * compare binary
		 * @return true; false
		 * @param in bin instance
		 * @see bin::equal()
		 */
		cf::bool_t operator !=(const bin & in) const;

		/**
		 * get a byte
		 * @return a byte
		 * @param index index
		 */
		cf::uint8_t operator [](const cf::size_t index) const;

		/**
		 * get as string
		 * @return string
		 */
		std::string toString() const;

		/**
		 * dump binary to file-pointer
		 * @param prefix prefix for output
		 * @param limit [option] limit
		 * @param fp [option] file-pointer
		 */
		cf::void_t print(const cf::char_t * prefix = "",
						 const cf::size_t limit = -1,
						 FILE * fp = stdout) const;
	};
}

#endif // #ifndef __cf_bin_h__
