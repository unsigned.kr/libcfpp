/**
 * @file test.hpp
 * @author myusgun@gmail.com
 * @brief Testing Macros
 */
#ifndef __cf_test_hpp__
#define __cf_test_hpp__

#include "cf/exception.h"
#include "cf/util.h"

#include <iostream>
#include <stdio.h>

#define _SOURCE	"[" << __FILE__ << ":" << __LINE__ << "] "
#define P_ERROR(_e)	std::cout << _SOURCE << _e.stackTrace() << std::endl

#define TEST_CLASS_NAME(_TESTCASE,_TEST)		_TESTCASE##_##_TEST##_Test
#define TEST_FUNC_NAME(_TESTCASE,_TEST)		_TESTCASE##_##_TEST##_Run

class cfTest
{
private:
	cf::int32_t mArgc;
	cf::char_t ** mArgv;
	cf::int32_t mNumberOfTest;
	cf::int32_t mFailed;
	cf::int32_t mTime;
	cf::bool_t mIsStackPrint;

	cfTest() : mArgc(0), mArgv(NULL), mNumberOfTest(0), mFailed(0), mTime(0), mIsStackPrint(false) {
		std::cout << "[==========] Begin" << std::endl;
		std::cout << "[----------] Preparing" << std::endl;
	}

	~cfTest() {
		std::cout << "[----------] " << mTime << " ms total" << std::endl;
		std::cout << "[  PASSED  ] " << mNumberOfTest - mFailed << " tests" << std::endl;
		std::cout << "[  FAILED  ] " << mFailed << " tests " << std::endl;
		std::cout << "[==========] End" << std::endl;
	}

public:
	static cfTest & getInstance() {
		static cfTest instance;
		return instance;
	}

	cf::void_t init(cf::int32_t argc, cf::char_t ** argv) { mArgc = argc; mArgv = argv; }
	cf::void_t regist() { ++mNumberOfTest; }
	cf::void_t failed() { ++mFailed; }
	cf::void_t time(cf::int32_t ms) { mTime += ms; }
	cf::int32_t result() const { return mFailed; }
	cf::void_t printStack(const cf::bool_t flag) { mIsStackPrint = flag; }
	cf::bool_t printStack() { return mIsStackPrint; }
	cf::int32_t getArgumentCount() { return mArgc; }
	cf::char_t * getArgumentValue(int idx) { return mArgv[idx]; }
};

#define TEST_INIT(_c,_v)	cfTest::getInstance().init(_c,_v)
#define TEST_RESULT()		cfTest::getInstance().result()
#define TEST_ARGC			cfTest::getInstance().getArgumentCount()
#define TEST_ARGV(_i)		cfTest::getInstance().getArgumentValue(_i)

#define GET_TIME(_c)		(_c.mHour * (60 * 60 * 1000) + _c.mMin * (60 * 1000) + _c.mSec * 1000 + _c.mUsec)

#define TEST(_TESTCASE,_TEST)	\
	cf::void_t TEST_FUNC_NAME(_TESTCASE,_TEST)() throw (cf::exception);	\
	class TEST_CLASS_NAME(_TESTCASE,_TEST) {	\
	private:	\
		cf::bool_t mStatus;	\
		cf::util::datetime mBegin;	\
		cf::util::datetime mEnd;	\
		cf::int32_t mRunTime;	\
	public:	\
		TEST_CLASS_NAME(_TESTCASE,_TEST)() : mStatus(true), mRunTime(0) {	\
			cfTest::getInstance().regist();	\
			std::cout << "[ RUN      ] " << #_TESTCASE << "." << #_TEST << std::endl;	\
			try { mBegin = cf::util::getInstance()->getDateTime(); } catch (cf::exception &e) { P_ERROR(e); }	\
			try { TEST_FUNC_NAME(_TESTCASE, _TEST)();              } catch (cf::exception &e) { P_ERROR(e); mStatus = false; }	\
			try { mEnd = cf::util::getInstance()->getDateTime();   } catch (cf::exception &e) { P_ERROR(e); }	\
			cf::int32_t begin = GET_TIME(mBegin);	\
			cf::int32_t end = GET_TIME(mEnd);	\
			mRunTime = end - begin;	\
			cfTest::getInstance().time(mRunTime);	\
			if (mStatus) {	\
				std::cout << "[       OK ] ";	\
			} else {	\
				std::cout << "[  FAILED  ] ";	\
				cfTest::getInstance().failed();	\
			}	\
			std::cout << #_TESTCASE << "." << #_TEST << " (" << mRunTime << " ms)" << std::endl;	\
		}	\
		cf::int32_t getTime() const { return mRunTime; }	\
	};	\
	cf::void_t TEST_FUNC_NAME(_TESTCASE,_TEST)() throw (cf::exception)

#define TEST_RUN(_TESTCASE,_TEST)	\
	TEST_CLASS_NAME(_TESTCASE,_TEST)	_TESTCASE##_TEST##_inst;	\

#define GET_TIME_OF_TEST(_TESTCASE,_TEST)	\
	_TESTCASE##_TEST##_inst.getTime()

#define ASSERT_NO_throw (_SENTENCE)	\
	try { _SENTENCE; } catch(cf::exception &e) { P_ERROR(e); return; }

#define ASSERT_EXCEPTION(_SENTENCE)	\
	try { _SENTENCE; } catch(cf::exception &e) { if(cfTest::getInstance().printStack()) P_ERROR(e); else std::cout << _SOURCE << e.what() << std::endl; }

#define PRINT_STACK(_b)	\
	cfTest::getInstance().printStack(_b)

#define ASSERT_EQ(_lhs,_rhs)	\
	if ((_lhs) != (_rhs))	CF_THROW(#_lhs << " != " << #_rhs)

#endif // #ifndef __cf_test_hpp__
