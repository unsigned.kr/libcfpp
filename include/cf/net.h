/**
 * @file net.h
 * @author myusgun@gmail.com
 * @brief network
 */
#ifndef __cf_tcp_h__
#define __cf_tcp_h__

#include "cf/exception.h"
#include "cf/bin.h"

#include <string>

#define UNUSED_SOCKET	-1

namespace cf
{
	/**
	 * network
	 */
	namespace net
	{
		/**
		 * network host
		 */
		class host
		{
		private:
			std::string mAddress;
			cf::uint16_t mPort;

		public:
			/**
			 * constructor
			 * @param address host address
			 * @param port port number
			 */
			host(const std::string & address,
				 const cf::uint16_t port);

			/**
			 * get host address
			 * @return host address
			 */
			const std::string & address() const;

			/**
			 * get port number
			 * @return port number
			 */
			cf::uint16_t port() const;
		};

		/**
		 * TCP Socket
		 */
		class tcp
		{
		private:
			cf::socket_t mSocket;
			cf::int32_t mTimeout;

			/**
			 * receive
			 * @param out created-bin
			 * @throw cf::exception
			 */
			cf::void_t receive(bin & out) const
				throw (cf::exception);

		public:
			/**
			 * constructor
			 * @param attachedSocket [option] socket descriptor for attachment
			 * @throw cf::exception
			 */
			tcp(const cf::socket_t attachedSocket = cf::nodesc)
				throw (cf::exception);

			/**
			 * destructor
			 */
			virtual ~tcp();

			/**
			 * get socket descriptor
			 * @return socket descriptor
			 */
			cf::socket_t descriptor() const;

			/**
			 * close connection
			 */
			cf::void_t close();

			/**
			 * connect to host
			 * @param peer peer host
			 * @param timeout timeout
			 * @throw cf::exception
			 */
			cf::void_t connect(const host & peer,
							   const cf::int32_t timeout = 0)
				throw (cf::exception);

			/**
			 * connect to address:port
			 * @param address address
			 * @param port port
			 * @param timeout timeout
			 * @throw cf::exception
			 */
			cf::void_t connect(const std::string & address,
							   const cf::uint16_t port,
							   const cf::int32_t timeout = 0)
				throw (cf::exception);

			/**
			 * bind
			 * @param port port
			 * @throw cf::exception
			 */
			cf::void_t bind(const cf::uint16_t port) const
				throw (cf::exception);

			/**
			 * server ready
			 * @param backlog [option] backlog
			 * @throw cf::exception
			 */
			cf::void_t listen(const cf::int32_t backlog = 5) const
				throw (cf::exception);

			/**
			 * accept client
			 * @return an instance of tcp client
			 * @throw cf::exception
			 */
			tcp accept() const
				throw (cf::exception);

			/**
			 * attach socket
			 * @param sock socket descriptor
			 * @throw cf::exception
			 */
			cf::void_t attach(const cf::socket_t sock)
				throw (cf::exception);

			/**
			 * detach socket
			 * @return socket descriptor
			 * @throw cf::exception
			 */
			cf::socket_t detach()
				throw (cf::exception);

			/**
			 * send
			 * @param in data
			 * @throw cf::exception
			 */
			cf::void_t send(const bin & in) const
				throw (cf::exception);

			/**
			 * receive
			 * @return received data
			 * @param size expected data length
			 * @throw cf::exception
			 */
			bin receive(const cf::int32_t size) const
				throw (cf::exception);

			/**
			 * receive
			 * @return received all of data
			 * @throw cf::exception
			 */
			bin receive() const
				throw (cf::exception);

			/**
			 * get socket option
			 */
			cf::void_t getOption(const cf::int32_t optname,
								 cf::void_t * optval,
								 cf::int32_t * optlen) const
				throw (cf::exception);

			/**
			 * set socket option
			 */
			cf::void_t setOption(const cf::int32_t optname,
								 const cf::void_t * optval,
								 const cf::int32_t optlen) const
				throw (cf::exception);

			/**
			 * set non-blocking
			 * @param flag true; false
			 */
			cf::void_t setNonBlocking(const cf::bool_t flag);

			/**
			 * set timeout
			 * @param seconds timeout seconds
			 * @see setNonBlocking()
			 */
			cf::void_t setTimeout(const cf::int32_t seconds);

			/**
			 * get local host
			 * @return local host
			 * @see cf::network::host
			 */
			host local() const
				throw (cf::exception);

			/**
			 * get peer host
			 * @return peer host
			 * @see cf::network::host
			 */
			host peer() const
				throw (cf::exception);
		};

		/**
		 * NIC(Network Interface Card)
		 */
		class nic
		{
		public:
			/**
			 * get mac address
			 * @return mac address
			 * @throw cf::exception
			 */
			static std::string getMACAddress()
				throw (cf::exception);
		};

		/**
		 * byteOrder
		 */
		class byteOrder
		{
		public:
			/**
			 * host to network long
			 * @return long for network
			 * @param in long for host
			 */
			static cf::uint32_t htonl(cf::uint32_t in);

			/**
			 * host to network short
			 * @return short for network
			 * @param in short for host
			 */
			static cf::uint16_t htons(cf::uint16_t in);

			/**
			 * network to host long
			 * @return long for host
			 * @param in long for network
			 */
			static cf::uint32_t ntohl(cf::uint32_t in);

			/**
			 * network to host short
			 * @return short for host
			 * @param in short for network
			 */
			static cf::uint16_t ntohs(cf::uint16_t in);
		};
	}
}

#endif // #ifndef __cf_tcp_h__
