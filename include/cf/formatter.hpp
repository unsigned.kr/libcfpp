/**
 * @file formatter.hpp
 * @author myusgun@gmail.com
 * @brief Formatter
 */
#ifndef __cf_formatter_hpp__
#define __cf_formatter_hpp__

#include <sstream>
#include <string>

/**
 * make string using Formatter
 * @see Formatter
 */
#define STR(_x)	(cf::formatter() << _x).str()

namespace cf
{
	/**
	 * a wrapper-class of std::ostringstream for temporary instance
	 * @see STR() macro
	 * @remarks http://stackoverflow.com/questions/303562/c-format-macro-inline-ostringstream
	 */
	class formatter
	{
	private:
		std::ostringstream mOSS;

	public:
		template<typename T>
		formatter & operator << (const T & v)
		{
			mOSS << v;
			return *this;
		}

		formatter & operator << (cf::char_t * v)
		{
			mOSS << (v ? v : "null");
			return *this;
		}

		formatter & operator << (const cf::char_t * v)
		{
			mOSS << (v ? v : "null");
			return *this;
		}

		std::string str()
		{
			return mOSS.str();
		}
	};
}

#endif // #ifndef __cf_formatter_hpp__
