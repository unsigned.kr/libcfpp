#include "cf/test.hpp"
#include "cf/bin.h"
#include "cf/codec.h"
#include "cf/file.h"
#include "cf/logger.h"
#include "cf/task.h"
#include "cf/util.h"
#include "cf/net.h"

#include <string>

/*--------------------------------------------------------------*/

// test string
#define STRING	"test string"
#define LOGFILE	"lol"
// for networking
#define HOST	"localhost"
#define PORT	1234
#define TIMEOUT	3

/*--------------------------------------------------------------*/

static void throwEx2() throw (cf::exception);
static void throwEx1() throw (cf::exception);
static int serverWorker(void * arg);
static int clientWorker(void * arg);

/*--------------------------------------------------------------*/

cf::task::mutex gMutex;
cf::task::thread gServerWorker(serverWorker);

/*--------------------------------------------------------------*/

TEST(Exception, main)
{
	try
	{
		throwEx1();
	}
	catch (cf::exception & e)
	{
		std::cerr << e.stackTrace();
	}
}

TEST(Bin, main)
{
	cf::bin a, b(STRING);

	a = b;

	cf::bin c = b;

	b.print("bin-value");
}

TEST(Codec, main)
{
	std::string e;
	cf::bin d;
	cf::codec::ICodec * codec = NULL;

	codec = cf::codec::base64::getInstance();;
	e = codec->encode(cf::bin(STRING));
	d = codec->decode(e);
	std::cout << "input   : " << STRING << "\n"
			  << "encoded : " << e      << "\n";
	d.print("decoded");

	codec = cf::codec::hex::getInstance();;
	e = codec->encode(cf::bin(STRING));
	d = codec->decode(e);
	std::cout << "input   : " << STRING << "\n"
			  << "encoded : " << e      << "\n";
	d.print("decoded");
}

TEST(File, main)
{
	cf::file f(STRING);

	f.open(cf::file::CREATE | cf::file::RW | cf::file::LOCK);
	f.write(cf::bin(STRING));
	f.close();
	f.remove();
}

TEST(Logger, main)
{
	enum LOG_LEVEL
	{
		ERR = 1,
		WARN,
		INFO,
		DEBUG
	};
	cf::logger * log = cf::logger::getInstance();

	log->init(".");
	log->setLevel(10);

	log->add(LOGFILE, ERR  , "ERROR"   , cf::logger::DAILY);
	log->add(LOGFILE, WARN , "WARNNING", cf::logger::DAILY);
	log->add(LOGFILE, INFO , "INFO"    , cf::logger::DAILY);
	log->add(LOGFILE, DEBUG, "DEBUG"   , cf::logger::DAILY);

	log->write(ERR, "this is log message");
}

TEST(Util, main)
{
	cf::util::datetime dt = cf::util::getInstance()->getDateTime();

	std::cout << "date : "
			  << dt.mYear << ":" << dt.mMonth << ":" << dt.mDay
			  << "(" << dt.mWeek << ":" << dt.mWeekName << ")"
			  << std::endl;
	std::cout << "time : "
			  << dt.mHour << ":" << dt.mMin << ":" << dt.mSec << "." << dt.mUsec
			  << std::endl;
}

TEST(ThreadNetworking, Server)
{
	std::cout << "opening " << PORT << " ..." << std::endl;

	gServerWorker.start(NULL);

	// wait for server-binding
	cf::task::thread::sleep(10);

	std::cout << "server worker was started" << std::endl;
}

TEST(ThreadNetworking, Client)
{
	const int max = 5;
	int i = 0;

	cf::task::thread worker[max] = {
		cf::task::thread(clientWorker),
		cf::task::thread(clientWorker),
		cf::task::thread(clientWorker),
		cf::task::thread(clientWorker),
		cf::task::thread(clientWorker),
	};

	for (i = 0 ; i < max ; i++)
		worker[i].start(NULL);

	for (i = 0 ; i < max ; i++)
		worker[i].join();
}

TEST(Networking, NIC)
{
	std::cout << "H/W Address : "
			  << cf::net::nic::getMACAddress()
			  << std::endl;
}

int main(int argc, char ** argv)
{
	TEST_INIT(argc, argv);

	TEST_RUN(Exception       , main);
	TEST_RUN(Bin             , main);
	TEST_RUN(Codec           , main);
	TEST_RUN(File            , main);
	TEST_RUN(Logger          , main);
	TEST_RUN(Util            , main);
	TEST_RUN(ThreadNetworking, Server);
	TEST_RUN(ThreadNetworking, Client);
	TEST_RUN(Networking      , NIC);

	TEST_RESULT();

	return 0;
}

/*--------------------------------------------------------------*/

void throwEx2() throw (cf::exception)
{
	THROW_EXCEPTION("exception test");
}

void throwEx1() throw (cf::exception)
{
	try
	{
		throwEx2();
	}
	catch (cf::exception & e)
	{
		FORWARD_EXCEPTION(e);
	}
}

int serverWorker(void * arg)
{
	try
	{
		cf::net::tcp server;
		cf::net::tcp client;
		cf::bin msg;

		server.bind(PORT);
		server.listen();

		while (true)
		{
			client.attach(server.accept().detach());
			cf::net::host peer = client.peer();
			std::cout << "client <"
					  << peer.address() << ":" << peer.port()
					  << "> is connected"
					  << std::endl;
			msg = client.receive();
			client.send(msg);
			client.close();
		}

		server.close();
	}
	catch (cf::exception & e)
	{
		std::cerr << e.stackTrace();
	}

	return 0;
}

int clientWorker(void * arg)
{
	std::cout << "connecting to "
			  << HOST << ":" << PORT << " ..."
			  << std::endl;

	try
	{
		cf::net::tcp client;
		cf::net::host host(HOST, PORT);

		cf::ulong_t tid = cf::task::thread::id();
		cf::bin msg(STR(STRING << " " << tid).c_str());
		cf::bin response;

		client.connect(host, TIMEOUT);
		client.send(msg);
		response = client.receive();
		client.close();

		SYNCHRONIZED(gMutex)
		{
			response.print("print response in synchronized-scope : ");
		}
	}
	catch (cf::exception & e)
	{
		SYNCHRONIZED(gMutex)
		{
			std::cerr << e.stackTrace() << cf::exception::systemMessage() << std::endl;
		}
	}

	return 0;
}
