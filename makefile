#----------------------------------------------------------
# platform
#----------------------------------------------------------
include ./makeinclude/userdefine.mk
include ./makeinclude/platform.mk

TARGET	= \
		  src	\
		  doc	\

#----------------------------------------------------------
# label
#----------------------------------------------------------
.PHONY: $(TARGET)

all:
	make $(TARGET) mode=$@

clean:
	make $(TARGET) mode=$@

$(TARGET):
	cd $@; make ${mode}

