#------------------------------------------------------------
# set command to check os
#------------------------------------------------------------
UNAME_S := $(shell uname -s)
UNAME_M := $(shell uname -m)
UNAME_R := $(shell uname -r)
UNAME_V := $(shell uname -v)
UNAME_A := $(shell uname -a)

#------------------------------------------------------------
# additional info.
#------------------------------------------------------------
MACHINE_INFO		= $(UNAME_S).$(UNAME_R)
ifeq ($(UNAME_S),AIX)
	MACHINE_INFO	= $(UNAME_S).$(UNAME_V).$(UNAME_R)
endif
ifeq ($(UNAME_S),Linux)
	MACHINE_INFO	= $(UNAME_S).$(UNAME_M)
endif

DATETIME			:= $(shell date +%Y%m%d)

#------------------------------------------------------------
# set options
#------------------------------------------------------------
ifeq ($(DEBUG),yes)
	DEFS = -D_DEBUG
	FLAG = -g
else
	DEFS = 
	FLAG = -O2
endif

EXT_SHARED	= so
EXT_STATIC	= a
EXT_EXECUTE	= ex

#------------------------------------------------------------
# set platform
#------------------------------------------------------------
ifeq ($(UNAME_S),SunOS)
	OS_FAMILY = solaris
	ifeq ($(UNAME_M),i86pc)
		ifeq ($(VER),32)
			OS_DEF = solaris_x86
		else
			OS_DEF = solaris_x86_64
		endif
	else
		ifeq ($(VER),32)
			OS_DEF = solaris
		else
			OS_DEF = solaris64
		endif
	endif
# java
	ifeq ($(UNAME_M),i86pc)
		JAVA_HOME = /usr/j2se
	else
		JAVA_HOME = /usr/java1.4.2
	endif
endif

ifeq ($(UNAME_S),AIX)
	OS_FAMILY = aix
	ifeq ($(VER),32)
		OS_DEF = aix
		JAVA_HOME = /usr/java14
	else
		OS_DEF = aix64
		JAVA_HOME = /usr/java14_64
	endif
# java
endif

ifeq ($(UNAME_S),HP-UX)
	OS_FAMILY = hp-ux
	ifeq ($(UNAME_M),ia64)
		UNAME_S := HP-UX_IA
		ifeq ($(VER),32)
			OS_DEF = hp_ia
		else
			OS_DEF = hp_ia64
		endif
	else
		ifeq ($(VER),32)
			OS_DEF = hp
		else
			OS_DEF = hp64
		endif
	endif
# java
	JAVA_HOME = /opt/java1.4
endif

ifeq ($(UNAME_S),Linux)
	OS_FAMILY = linux
	ifeq ($(UNAME_M),ppc64)
		OS_DEF = linux_ppc64
	else
		ifeq ($(VER),32)
			OS_DEF = linux
		else
			OS_DEF = linux_x86_64
		endif
	endif
# java
	ifeq ($(VER),32)
		JAVA_HOME := ${JAVA_HOME}
	endif
endif

ifeq ($(UNAME_S),Darwin)
	OS_FAMILY = mac
	ifeq ($(VER),32)
		OS_DEF = mac
	else
		OS_DEF = mac_x86_64
	endif
endif

ifeq ($(filter $(UNAME_A),Cygwin),Cygwin)
	# forced
	VER			= 32
	EXT_SHARED	= dll
	EXT_STATIC	= lib
	EXT_EXECUTE	= exe

	# run as linux
	OS_DEF		= linux
endif

ifeq ($(JAVA_HOME),)
	JAVA_HOME = $(USER_JAVA_HOME)
endif
ifeq ($(JAVA_HOME),)
$(error JAVA_HOME=\'\'.)
endif

#------------------------------------------------------------
# set build env.
#------------------------------------------------------------
#ifeq ($(OS_DEF),$(filter $(OS_DEF),solaris solaris64 solaris_x86 solaris_x86_64))
ifeq ($(OS_FAMILY),solaris)
	ifeq ($(OS_DEF),solaris_x86_64)
		OPT_SHARED_LDFLAGS	+= -shared
	else
		OPT_SHARED_LDFLAGS	+= -G
	endif

CC				= gcc
CXX				= g++
LD				= $(CXX)
AR				= ar
CDEFS			= -D_SOLARIS -D_REENTRANT $(DEFS)
CFLAGS			= $(FLAG) -m$(VER) -Wall -fPIC -Wconversion -Wpointer-arith -Wcast-align
LDFLAGS			= $(FLAG) -m$(VER) -Wall
SHARED_LDFLAGS	= $(LDFLAGS) $(OPT_SHARED_LDFLAGS) -shared
ARFLAGS			= rc
LIBS			= -lnsl -lsocket -lpthread
LIBENV			= LD_LIBRARY_PATH
COMPILER_VER	= $(CXX) --version | awk 'NR==1'
endif

ifeq ($(OS_FAMILY),aix)
# -Wl,-brtl
# => http://durgaprasad.wordpress.com/2006/09/28/problems-with-linking-of-shared-libraries-in-aix/
CC				= /usr/vac/bin/xlc_r
CXX				= /usr/vacpp/bin/xlC_r
LD				= $(CXX)
AR				= ar
CDEFS			= -D_AIX -D_REENTRANT -D_POSIX_THREADS=1 -D_PTHREADS -D_THREAD_SAFE -D_POSIX_C_SOURCE=199506L $(DEFS)
CFLAGS			= $(FLAG) -q$(VER) -qpic=large -qflag=w:w -qeh=v6 -qchars=signed -qstaticinline
LDFLAGS			= $(FLAG) -q$(VER) -qpic=large -brtl
SHARED_LDFLAGS	= $(LDFLAGS) -G -qmkshrobj -bnoentry -bM:SRE
ARFLAGS			= -X64 -ruv
LIBS			= -lc -lpthread
LIBENV			= LIBPATH
COMPILER_VER	= lslpp -l | grep xlC | grep rte | grep -v msg

# g++ {{{
ifeq ($(compiler),g++)
CC				= g++
CXX				= g++
LD				= $(CXX)
AR				= ar
CDEFS			= -D_AIX -D_REENTRANT -D_POSIX_THREADS=1 -D_PTHREADS -D_THREAD_SAFE -D_POSIX_C_SOURCE=199506L $(DEFS)
CFLAGS			= $(FLAG) -maix$(VER)
LDFLAGS			= $(FLAG) -maix$(VER) -Wl,-brtl
SHARED_LDFLAGS	= $(LDFLAGS) -shared
ARFLAGS			= -X64 -ruv
LIBS			= -lc -lpthread
LIBENV			= LIBPATH
COMPILER_VER	= $(CXX) --version | awk 'NR==1'
endif
# }}} g++
endif

ifeq ($(OS_FAMILY),hp-ux)

	ifneq ($(DEBUG),yes)
		FLAG = +O2
	endif

# in pa-risc, +W690 option is disable a warning about header without line-feed.
	ifeq ($(OS_DEF),hp)
		EXT_SHARED			= sl
		HP_LD				= /opt/aCC/bin/aCC
		FLAG				+= -AA -mt +W690 +Z +DAportable
		OPT_SHARED_LDFLAGS	= +DAportable
	endif

	ifeq ($(OS_DEF),hp64)
		EXT_SHARED			= sl
		HP_LD				= /opt/aCC/bin/aCC
		FLAG				+= -AA -mt +W690 +z +DA2.0W
		OPT_SHARED_LDFLAGS	= +DA2.0W
	endif

# in itanium64, +W2001 option is disable a warning about header without line-feed.
	ifeq ($(OS_DEF),hp_ia)
		HP_LD				= ld
		FLAG				+= +DD32 +W2001
		OPT_SHARED_LDFLAGS	= +s +vallcompatwarnings -B symbolic
	endif

	ifeq ($(OS_DEF),hp_ia64)
		HP_LD				= ld
		FLAG				+= +DD64 +W2001
		OPT_SHARED_LDFLAGS	= +s +vallcompatwarnings -B symbolic
	endif

CC				= cc
CXX				= /opt/aCC/bin/aCC
LD				= $(HP_LD)
AR				= ar
CDEFS			= -D_HPUX -D_HPUX_SOURCE -D_REENTRANT -D_POSIX_THREADS=1 -D_POSIX_C_SOURCE=199506L $(DEFS)
CFLAGS			= $(FLAG) -q -n -z
LDFLAGS			= $(FLAG) -q -n -Wl,+s
SHARED_LDFLAGS	= $(HP_SHARED_LDFLAGS) -b
ARFLAGS			= cru
LIBS			= -lnsl -lc -lpthread -lstd_v2 -lCsup
LIBENV			= SHLIB_PATH
COMPILER_VER	= $(CXX) -V

# g++ {{{
ifeq ($(compiler),g++)

	ifeq ($(OS_DEF),hp)
		EXT_SHARED	= sl
	endif

	ifeq ($(OS_DEF),hp64)
		EXT_SHARED	= sl
	endif

	ifeq ($(OS_DEF),hp_ia)
		FLAG		= -milp32
		LDFLAGS		= 
	endif

	ifeq ($(OS_DEF),hp_ia64)
		FLAG		= -mlp64
		LDFLAGS		= 
	endif

CC				= gcc
CXX				= g++
LD				= g++
AR				= ar
CDEFS			= -D_HPUX -D_REENTRANT -DMTHREAD -DPTHREAD -D_THREAD_SAFE -D_POSIX_C_SOURCE=199506L $(DEFS)
CFLAGS			= $(FLAG) -Wall -fPIC -Wconversion -Wpointer-arith -Wcast-align
LDFLAGS			= $(FLAG) -Wall
SHARED_LDFLAGS	= $(LDFLAGS) -shared
ARFLAGS			= cru
LIBS			= -lnsl -lc -lpthread
LIBENV			= SHLIB_PATH
COMPILER_VER	= $(CXX) --version | awk 'NR==1'
endif
# }}} g++
endif

ifeq ($(OS_FAMILY),linux)

	ifneq ($(shell find /usr/include -name "stropts.h"),)
		DEFS	+= -D_HAVE_STROPTS_H
	endif

CC				= gcc
CXX				= g++
LD				= $(CXX)
AR				= ar
CDEFS			= -D_LINUX -D_REENTRANT $(DEFS)
CFLAGS			= $(FLAG) -m$(VER) -Wall -fPIC -Wconversion -Wpointer-arith -Wcast-align
LDFLAGS			= $(FLAG) -m$(VER) -Wall
SHARED_LDFLAGS	= $(LDFLAGS) -shared -rdynamic
ARFLAGS			= rc
LIBS			= -lpthread $(USER_LIBS)
LIBENV			= LD_LIBRARY_PATH
COMPILER_VER	= $(CXX) --version | awk 'NR==1'
endif

ifeq ($(OS_FAMILY),mac)
CC				= gcc
CXX				= g++
LD				= $(CXX)
AR				= ar
CDEFS			= -D_MAC -D_REENTRANT $(DEFS)
CFLAGS			= $(FLAG) -m$(VER) -Wall -fPIC -Wconversion -Wpointer-arith -Wcast-align
LDFLAGS			= $(FLAG) -m$(VER) -Wall
SHARED_LDFLAGS	= $(LDFLAGS) -shared -rdynamic
ARFLAGS			= rc
LIBS			= -lpthread
LIBENV			= LD_LIBRARY_PATH
COMPILER_VER	= $(CXX) --version | awk 'NR==1'
endif
