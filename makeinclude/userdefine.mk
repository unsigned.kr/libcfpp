#------------------------------------------------------------
# lower-case keys for lowercase-command-line option
#------------------------------------------------------------

#------------------------------------------------------------
# xecuredb
#------------------------------------------------------------ 
# os-version
# > 32 or 64
ver					= 64
VER					= $(ver)

# compile with debugging flag and definition
# > yes or no
debug				= yes
DEBUG				= $(debug)

# product
PRODUCT_NAME		= libcf++
PRODUCT_VERSION		= 0.1dev

# lib type
# > shared or static
libtype				= static
LIBTYPE				= $(libtype)

# documentation
DOCUMENT_PATH		= ../_doc

# path
BUILD_PATH			= ../_build

#------------------------------------------------------------
# external tools or libraries
#------------------------------------------------------------
# java path
with-jni			= no
WITH_JNI			= $(with-jni)
USER_JAVA_HOME		= ${HOME}/workspace/java/jdk1.5.0_12

# link standard-library statically
# ex) libstdc++, libgcc ?
# > yes or no
static-std			= no
STATIC_STD			= $(static-std)

#------------------------------------------------------------
# user definitions and options
#------------------------------------------------------------
# options for exp
# > yes or no

# build-environment
ifeq ($(STATIC_STD),yes)
# not yet
	USER_LDFLAGS	+= -static-libgcc
	USER_LIBS		+= $(shell g++ -print-file-name=libstdc++.a)
endif

