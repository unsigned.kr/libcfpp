#------------------------------------------------------------
# set command to check os
#------------------------------------------------------------
UNAME_S := $(shell uname -s)
UNAME_M := $(shell uname -m)
UNAME_R := $(shell uname -r)
UNAME_V := $(shell uname -v)
UNAME_A := $(shell uname -a)

#------------------------------------------------------------
# set options
#------------------------------------------------------------
ifeq ($(DEBUG), yes)
	DEFS = -D_DEBUG
	FLAG = -g
else
	DEFS = 
	FLAG = -O2
endif

EXT_SHARED	= so
EXT_STATIC	= a
EXT_EXECUTE	= ex

#------------------------------------------------------------
# set platform
#------------------------------------------------------------
ifeq ($(UNAME_S), SunOS)
	OS_FAMILY = solaris
	ifeq ($(UNAME_M), i86pc)
		ifeq ($(VER), 32)
			OS_DEF = solaris_x86
		else
			OS_DEF = solaris_x86_64
		endif
	else
		ifeq ($(VER), 32)
			OS_DEF = solaris
		else
			OS_DEF = solaris64
		endif
	endif
# java
	ifeq ($(UNAME_M), i86pc)
		JAVA_HOME = /usr/j2se
	else
		JAVA_HOME = /usr/java1.4.2
	endif
endif

ifeq ($(UNAME_S), AIX)
	OS_FAMILY = aix
	ifeq ($(VER), 32)
		OS_DEF = aix
		JAVA_HOME = /usr/java14
	else
		OS_DEF = aix64
		JAVA_HOME = /usr/java14_64
	endif
# java
endif

ifeq ($(UNAME_S), HP-UX)
	OS_FAMILY = hp-ux
	ifeq ($(UNAME_M), ia64)
		UNAME_S := HP-UX_IA
		ifeq ($(VER), 32)
			OS_DEF = hp_ia
		else
			OS_DEF = hp_ia64
		endif
	else
		ifeq ($(VER), 32)
			OS_DEF = hp
		else
			OS_DEF = hp64
		endif
	endif
# java
	JAVA_HOME = /opt/java1.4
endif

ifeq ($(UNAME_S), Linux)
	OS_FAMILY = linux
	ifeq ($(UNAME_M), ppc64)
		OS_DEF = linux_ppc64
	else
		ifeq ($(VER), 32)
			OS_DEF = linux
		else
			OS_DEF = linux_x86_64
		endif
	endif
# java
	#----------------------------------------
	# for [myusgun@dev.softforum.com] -_-;
	#----------------------------------------
	ifeq ($(VER), 32)
		JAVA_HOME = ${HOME}/java/j2sdk1.4.2_19
	endif
endif

ifeq ($(UNAME_S), Darwin)
	OS_FAMILY = mac
	ifeq ($(VER), 32)
		OS_DEF = mac
	else
		OS_DEF = mac_x86_64
	endif
endif

ifeq ($(filter $(UNAME_A),Cygwin),Cygwin)
	# forced
	VER			= 32
	EXT_SHARED	= dll
	EXT_STATIC	= lib
	EXT_EXECUTE	= exe

	# run as linux
	OS_DEF		= linux
endif

ifeq ($(JAVA_HOME),)
	JAVA_HOME = $(USER_JAVA_HOME)
endif
ifeq ($(JAVA_HOME),)
$(error JAVA_HOME=\'\'.)
endif

#------------------------------------------------------------
# set build env.
#------------------------------------------------------------
#ifeq ($(OS_DEF), $(filter $(OS_DEF),solaris solaris64 solaris_x86 solaris_x86_64))
ifeq ($(OS_FAMILY), solaris)
CC				= gcc
LD				= gcc
AR				= ar
CDEFS			= $(DEFS) -D_SOLARIS -D_REENTRANT
CFLAGS			= $(FLAG) -m$(VER) -Wall -Wconversion -Wpointer-arith -Wcast-align -fPIC
LDFLAGS			= -m$(VER)
SHARED_FLAGS	= $(LDFLAGS)
ARFLAGS			= rc
LIBS			= -lnsl -lsocket -lpthread
LIBENV			= LD_LIBRARY_PATH

	ifeq ($(OS_DEF), solaris_x86_64)
		SHARED_FLAGS	+= -shared
	else
		SHARED_FLAGS	+= -G
	endif
endif

ifeq ($(OS_FAMILY), aix)
CC				= cc_r
LD				= ld
AR				= ar
CDEFS			= $(DEFS) -D_AIX -D_REENTRANT -D__STDC__=1
CFLAGS			= $(FLAG) -q$(VER) -qcpluscmt
LDFLAGS			= -b$(VER) -brtl
SHARED_FLAGS	= $(LDFLAGS) -bnoentry -bM:SRE -bexpall
ARFLAGS			= cru
LIBS			= -lc -lpthread
LIBENV			= LIBPATH
endif

ifeq ($(OS_FAMILY), hp-ux)
CC				= cc
LD				= ld
AR				= ar
CDEFS			= $(DEFS) -D_HPUX -D_REENTRANT
CFLAGS			= $(FLAG) +z -q -n -w1 -Ae -z +s
LDFLAGS			= 
SHARED_FLAGS	= $(LDFLAGS) -b +s
ARFLAGS			= cru
LIBS			= -lc -lpthread
LIBENV			= SHLIB_PATH

	ifeq ($(OS_DEF), hp)
		CFLAGS		+= +DAportable
		EXT_SHARED	= sl
	endif

	ifeq ($(OS_DEF), hp64)
		CFLAGS		+= +DA2.0W
		EXT_SHARED	= sl
	endif

	ifeq ($(OS_DEF), hp_ia)
		CFLAGS		+= +DA1.1 +s
	endif

	ifeq ($(OS_DEF), hp_ia64)
		CFLAGS	+= +DD64 +w1 -Ae -z +u4
	endif
endif

ifeq ($(OS_FAMILY), linux)
CC				= gcc
LD				= gcc
AR				= ar
CDEFS			= $(DEFS) -D_LINUX -D_REENTRANT
CFLAGS			= $(FLAG) -m$(VER) -Wall -Wconversion -Wpointer-arith -Wcast-align -fPIC
LDFLAGS			= -m$(VER)
SHARED_FLAGS	= $(LDFLAGS) -shared
ARFLAGS			= rc
LIBS			= -lpthread
LIBENV			= LD_LIBRARY_PATH
endif

ifeq ($(OS_FAMILY), mac)
CC				= gcc
LD				= gcc
AR				= ar
CDEFS			= $(DEFS) -D_MAC -D_REENTRANT
CFLAGS			= $(FLAG) -m$(VER) -Wall -Wconversion -Wpointer-arith -Wcast-align -fPIC
LDFLAGS			= -m$(VER)
SHARED_FLAGS	= $(LDFLAGS) -shared
ARFLAGS			= rc
LIBS			= 
LIBENV			= 
endif
